function bmiCalculator(weight, height) {
    bmi = weight / (height * height)
    if (bmi < 18.5) {
        interpretation = `Your BMI is ${bmi.toFixed(0)}, so you are underweight.`
    }
    else if (bmi >= 18.5 && bmi <= 24.9) {
        interpretation = `Your BMI is ${bmi.toFixed(0)}, so you have a normal weight.`
    }
    else if (bmi > 24.9) {
        interpretation = `Your BMI is ${bmi.toFixed(0)}, so you are overweight.`
    }
    return interpretation;
}

console.log(bmiCalculator(60, 2))