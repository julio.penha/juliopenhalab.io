function isLeap(year) {
    if (year % 4 == 0) {
        if (year % 100 == 0 && year % 400 != 0) {
            return false;
        }
        return true;
    }
    else return false;
}

console.log("2024 is leap?", isLeap(2024));