function carregar() {
    var msg = window.document.getElementById('msg')
    var img = window.document.getElementById('imagem')
    var data = new Date()
    var hora = data.getHours() + ":" + data.getMinutes().toLocaleString('pt-BR', { minimumIntegerDigits: 2 })

    msg.innerHTML = `Agora são ${hora}`

    if (hora >= 6 && hora < 12) {
        // Bom dia e5b37d
        img.src = 'img/fotomanha.png'
        document.body.style.background = '#e5b37d'
    } else if (hora >= 12 && hora <= 18) {
        // Boa tarde f6b558
        img.src = 'img/fototarde.png'
        document.body.style.background = '#91a1b9'
    }
    else {
        // Boa noite 081b1f
        img.src = 'img/fotonoite.png'
        document.body.style.background = '#081b1f'
    }

}