function parimpar(n) {
    if (n % 2 == 0) {
        return "par"
    }
    else { return "impar" }
}

function soma(a = 0, b = 0) {
    return a + b
}

let v = function (x) {
    return x * 2
}

function fat(n) {
    let f = 1
    for (let c = n; c > 1; c--) {
        f *= c
    }
    return f
}

function fatrec(n) {
    if (n == 1) {
        return 1
    }
    else {
        return n * fatrec(n - 1)
    }
}

console.log(parimpar(243))
console.log(soma(3))
console.log(v(5))
console.log(fat(5))
console.log(fatrec(5))