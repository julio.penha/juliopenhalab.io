function contar() {
    var txt_inicio = window.document.getElementById('txtinicio')
    var txt_fim = window.document.getElementById('txtfim')
    var txt_passo = window.document.getElementById('txtpasso')
    var res = document.querySelector("div#res")

    if (txt_inicio.value.length == 0 || txt_fim.value.length == 0 || txt_passo.value.length == 0) {
        res.innerHTML = "Impossível contar"
    } else if (!txt_inicio.value.length > 0) {
        alert('O valor de início precisa ser maior que zero')
    } else {
        var inicio = Number(txt_inicio.value)
        var fim = Number(txt_fim.value)
        var passo = Number(txt_passo.value)
        if (passo <= 0) {
            alert("Passo inválido, considerando passo = 1")
            passo = 1
        }
        res.innerHTML = "Contando:<br>"

        if (inicio > fim) {
            for (var i = inicio; i >= fim; i -= passo) {
                res.innerHTML += `${i} \u{1f449}`
            }
        }
        else {
            for (var i = inicio; i <= fim; i += passo) {
                res.innerHTML += `${i} \u{1f449}`
            }
        }
        res.innerHTML += ` \u{1f3c1}`
    }

}